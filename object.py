from enum import Enum
import random
import pygame

grap = pygame.image.load('graphics.GIF')
class Typ(Enum):
    boulder = 0
    diamond = 1
    magicWall = 2
    brickWall = 3
    steelWall = 4
    expandingWall = 5
    rockford = 6
    dirt = 7
    firefly = 8
    butterfly = 9
    ameoba = 10
    door = 11
    space = -1
class GameState(Enum):
    menu = 0
    playing = 1
    win = 2
    lose = 3
    exit = 4
    reset = 5
class RockfordState(Enum):
    none = -1
    goingLeft = 0
    goingRight = 1
    goingUp = 2
    goingDown = 3
class ChainExplosionAction(Enum):
    consumed = 0
    unaffected = 1
class State(Enum):
    dormant = 0
    on = 1
    expired =2


class Door:
    scanned = False
    typ = Typ.door
    impactExplosive = False
    chainExplosionAction = ChainExplosionAction.unaffected
    rounded = False
    opened = False
    def __init__(self, player):
        self.player = player
    def render(self,screen,x, y):
        if not self.opened:
            screen.blit(grap, (x*16, y*16), pygame.Rect((176, 80), (16,16)))
        else:
            screen.blit(grap, (x*16, y*16), pygame.Rect((32, 128), (16,16)))
    def step(self, table):
        if self.player.diamonds <= 0:
            self.opened = True

class Space:
    scanned = False
    typ = Typ.space
    rounded = False
    impactExplosive = False
    chainExplosionAction = ChainExplosionAction.consumed
    def render(self,screen,x, y):
        pass
    def step(self, table):
        pass
class Boulder:
    scanned = False
    def __init__(self, x, y):
        self.x = x
        self.y = y
    rounded = True
    impactExplosive = False
    falling = False
    chainExplosionAction = ChainExplosionAction.consumed
    typ = Typ.boulder
    def render(self,screen,x, y):
        screen.blit(grap, (x*16, y*16), pygame.Rect((56, 104), (16,16)))
    def step(self, table):
        flag = True
        if self.falling:
            if table[self.y-1][self.x].impactExplosive == True:
                table[self.y-1][self.x].explode()
                flag = False
            elif table[self.y-1][self.x].typ == Typ.magicWall:
                table[self.y][self.x] = Space()
                if MagicWall.state == State.dormant:
                    MagicWall.state = State.on
                if MagicWall.state == State.on:
                    if table[self.y-2][self.x] == Space():
                        table[self.y-2][self.x] = Diamond(x, y-2)
                        table[self.y-2][self.x].falling = True
                    flag = False

            elif table[self.y-1][self.x].typ != Typ.space:
                self.falling = False
                self.rounded = True
        if flag:
            if table[self.y-1][self.x].typ == Typ.space:
                table[self.y-1][self.x] = self
                table[self.y][self.x] = Space()
                self.y = self.y-1
                self.falling = True
                self.rounded = False
            elif table[self.y-1][self.x].rounded == True:
                if table[self.y-1][self.x-1].typ == Typ.space and table[self.y][self.x-1].typ == Typ.space:
                    table[self.y][self.x-1] = self
                    table[self.y][self.x] = Space()
                    self.x = self.x-1
                    self.falling = True
                    self.rounded = False

                elif table[self.y-1][self.x+1].typ == Typ.space and table[self.y][self.x+1].typ == Typ.space:
                    table[self.y][self.x+1] = self
                    table[self.y][self.x] = Space()
                    self.x = self.x+1
                    self.falling = True
                    self.rounded = False

class Diamond:
    scanned = False
    def __init__(self, x, y):
        self.x = x
        self.y = y
    rounded = True
    impactExplosive = False
    falling = False
    chainExplosionAction = ChainExplosionAction.consumed
    typ = Typ.diamond
    frame = 0
    def render(self,screen,x, y):
        screen.blit(grap, (x*16, y*16), pygame.Rect((8 + self.frame*24, 208), (16,16)))
        self.frame += 1
        if self.frame == 8:
            self.frame = 0
    def step(self, table):
        flag = True
        if self.falling:
            if table[self.y-1][self.x].impactExplosive == True:
                table[self.y-1][self.x].explode()
                flag = False
            elif table[self.y-1][self.x].typ == Typ.magicWall:
                table[self.y][self.x] = Space()
                if MagicWall.state == State.dormant:
                    MagicWall.state = State.on
                if MagicWall.state == State.on:
                    if table[self.y-2][self.x] == Space():
                        table[self.y-2][self.x] = Boulder(x, y-2)
                        table[self.y-2][self.x].falling = True
                    flag = False

            elif table[self.y-1][self.x].typ != Typ.space:
                self.falling = False
                self.rounded = True
        if flag:
            if table[self.y-1][self.x].typ == Typ.space:
                table[self.y-1][self.x] = self
                table[self.y][self.x] = Space()
                self.y = self.y-1
                self.falling = True
                self.rounded = False
            elif table[self.y-1][self.x].rounded == True:
                if table[self.y-1][self.x-1].typ == Typ.space and table[self.y][self.x-1].typ == Typ.space:
                    table[self.y][self.x-1] = self
                    table[self.y][self.x] = Space()
                    self.x = self.x-1
                    self.falling = True
                    self.rounded = False
                elif table[self.y-1][self.x+1].typ == Typ.space and table[self.y][self.x+1].typ == Typ.space:
                    table[self.y][self.x+1] = self
                    table[self.y][self.x] = Space()
                    self.x = self.x+1
                    self.falling = True
                    self.rounded = False
class MagicWall:
    scanned = False
    typ = Typ.magicWall
    impactExplosive = False    
    chainExplosionAction = ChainExplosionAction.consumed
    rounded = False
    state = State.dormant
    i = 1
    def render(self,screen,x, y):
        screen.blit(grap, (x*16, y*16), pygame.Rect((176, 104), (16,16)))
    def tick():
        if MagicWall.state == State.on:
            i -= 1
            if i == 0:
                MagicWall.state = State.expired
    def step(self, table):
        pass
class BrickWall:
    scanned = False
    typ = Typ.brickWall
    impactExplosive = False
    chainExplosionAction = ChainExplosionAction.consumed
    rounded = True
    def render(self,screen,x, y):
        screen.blit(grap, (x*16, y*16), pygame.Rect((176, 104), (16,16)))
    def step(self, table):
        pass
class SteelWall:
    scanned = False
    typ = Typ.steelWall
    impactExplosive = False
    chainExplosionAction = ChainExplosionAction.unaffected
    rounded = False
    def render(self,screen,x, y):
        screen.blit(grap, (x*16, y*16), pygame.Rect((176, 80), (16,16)))
    def step(self, table):
        pass
class ExpandingWall:
    scanned = False
    typ = Typ.expandingWall
    impactExplosive = False
    chainExplosionAction = ChainExplosionAction.consumed
    rounded = False
    def render(self,screen,x, y):
        screen.blit(grap, (x*16, y*16), pygame.Rect((176, 104), (16,16)))
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def step(self, table):
        if table[self.y][self.x-1].typ == Typ.space:
            table[self.y][self.x-1] = ExpandingWall(y, x-1)
            table[self.y][self.x-1].scanned = True
        if table[self.y][self.x+1].typ == Typ.space:
            table[self.y][self.x+1] = ExpandingWall(y, x+1,)
            table[self.y][self.x+1].scanned = True

        else:
            self.new = False
class Rockford:
    frame = 0
    scanned = False
    gameState = GameState.playing
    impactExplosive = True
    chainExplosionAction = ChainExplosionAction.consumed
    rounded = False
    typ = Typ.rockford
    state = RockfordState.none #rozne stany to rozne kierunki chodzenia - patrz Enum RockfordState
    notMoving = False #kopanie bez poruszania - kiedy jest nacisniety 'z'
    diamonds = 12
    tapping = False
    k = 0
    l = 0
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def explode(self):
        self.gameState = GameState.lose
    def render(self,screen,x, y):
        if self.frame == 8:
            self.frame = 0
        if self.state == RockfordState.goingLeft:
            self.k = 0
            screen.blit(grap, (x*16, y*16), pygame.Rect((208 + 24*self.frame, 104 + self.k*24), (16,16)))
        elif self.state == RockfordState.goingRight:
            self.k = 1
            screen.blit(grap, (x*16, y*16), pygame.Rect((208 + 24*self.frame, 104 + self.k*24), (16,16)))
        elif self.state == RockfordState.goingUp:
            screen.blit(grap, (x*16, y*16), pygame.Rect((208 + 24*self.frame, 104 + self.k*24), (16,16)))
        elif self.state == RockfordState.goingDown:
            screen.blit(grap, (x*16, y*16), pygame.Rect((208 + 24*self.frame, 104 + self.k*24), (16,16)))
        else:
            if self.frame == 0:
                if random.randint(0,15) == 0:
                    if self.tapping:
                        self.tapping = False
                    else:
                        self.tapping = True
                if random.randint(0,3) == 0:
                    if self.tapping:
                        self.l = 3
                    else:
                        self.l = 1
                elif self.tapping:
                    self.l = 2
                else:
                    self.l = 0
            if self.l == 0:
                screen.blit(grap, (x*16, y*16), pygame.Rect((208, 8), (16,16)))
            else:
                    screen.blit(grap, (x*16, y*16), pygame.Rect((208 + 24*self.frame, 8 + self.l*24), (16,16)))
        self.frame += 1
    def step(self, table):
        if self.state == RockfordState.goingLeft:
            if table[self.y][self.x-1].typ == Typ.dirt:
                table[self.y][self.x-1] = Space()
            elif table[self.y][self.x-1].typ == Typ.diamond:
                table[self.y][self.x-1] = Space()
                self.diamonds -= 1
                if self.diamonds < 0:
                    self.diamonds = 0
            elif table[self.y][self.x-1].typ == Typ.door:
                if table[self.y][self.x-1].opened:
                    self.gameState = GameState.win
            elif table[self.y][self.x-1].typ == Typ.boulder:
                if table[self.y][self.x-2].typ == Typ.space and table[self.y-1][self.x-1] != Typ.space:
                    if random.randint(0, 2) == 2:
                        table[self.y][self.x-2] = table[self.y][self.x-1]
                        table[self.y][self.x-1] = Space()
                        table[self.y][self.x-2].x = table[self.y][self.x-2].x -1
                        table[self.y][self.x+2].scanned = True


        elif self.state == RockfordState.goingRight:
            if table[self.y][self.x+1].typ == Typ.dirt:
                table[self.y][self.x+1] = Space()
            elif table[self.y][self.x+1].typ == Typ.diamond:
                table[self.y][self.x+1] = Space()
                self.diamonds -= 1
                if self.diamonds < 0:
                    self.diamonds = 0
            elif table[self.y][self.x+1].typ == Typ.door:
                if table[self.y][self.x+1].opened:
                    self.gameState = GameState.win
            elif table[self.y][self.x+1].typ == Typ.boulder:
                if table[self.y][self.x+2].typ == Typ.space and table[self.y-1][self.x+1] != Typ.space:
                    if random.randint(0, 2) == 2:
                        table[self.y][self.x+2] = table[self.y][self.x+1]
                        table[self.y][self.x+1] = Space()
                        table[self.y][self.x+2].x = table[self.y][self.x+2].x + 1
                        table[self.y][self.x+2].scanned = True
        elif self.state == RockfordState.goingUp:
            if table[self.y+1][self.x].typ == Typ.dirt:
                table[self.y+1][self.x] = Space()
            elif table[self.y+1][self.x].typ == Typ.diamond:
                table[self.y+1][self.x] = Space()
                self.diamonds -= 1
                if self.diamonds < 0:
                    self.diamonds = 0
            elif table[self.y+1][self.x].typ == Typ.door:
                if table[self.y+1][self.x].opened:
                    self.gameState = GameState.win
        elif self.state == RockfordState.goingDown:
            if table[self.y-1][self.x].typ == Typ.dirt:
                table[self.y-1][self.x] = Space()
            elif table[self.y-1][self.x].typ == Typ.diamond:
                table[self.y-1][self.x] = Space()
                self.diamonds -= 1
                if self.diamonds < 0:
                    self.diamonds = 0
            elif table[self.y-1][self.x].typ == Typ.door:
                if table[self.y-1][self.x].opened:
                    self.gameState = GameState.win
        if not self.notMoving:
            if self.state == RockfordState.goingLeft:
                if table[self.y][self.x-1].typ == Typ.space:
                    table[self.y][self.x-1] = self
                    table[self.y][self.x] = Space()
                    self.x = self.x-1
            elif self.state == RockfordState.goingRight:
                if table[self.y][self.x+1].typ == Typ.space:
                    table[self.y][self.x+1] = self
                    table[self.y][self.x] = Space()
                    self.x = self.x+1
            elif self.state == RockfordState.goingUp:
                if table[self.y+1][self.x].typ == Typ.space:
                    table[self.y+1][self.x] = self
                    table[self.y][self.x] = Space()
                    self.y = self.y+1
            elif self.state == RockfordState.goingDown:
                if table[self.y-1][self.x].typ == Typ.space:
                    table[self.y-1][self.x] = self
                    table[self.y][self.x] = Space()
                    self.y = self.y-1
class Dirt:
    scanned = False
    typ = Typ.dirt
    impactExplosive = False
    chainExplosionAction = ChainExplosionAction.consumed
    rounded = False
    def render(self,screen,x, y):
        screen.blit(grap, (x*16, y*16), pygame.Rect((80, 104), (16,16)))
    def step(self, table):
        pass