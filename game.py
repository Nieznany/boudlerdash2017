from object import *
import pygame
from pygame.locals import *


lose = pygame.image.load('Youlose.png')
win = pygame.image.load('Youwon.png')


def render(table1):
    screen.fill((0,0,0))
    for i in range(len(table1)-1, -1, -1):
        for z in range(0, len(table1[i])):
            j = table1[len(table1)-1-i][z]
            j.render(screen, z , i)

        print()
    pygame.display.flip()
def scan(table1):
    
    for i in range(len(table1)-1, -1, -1):
        for j in table1[i]:
            if not j.scanned:
                j.scanned = True
                j.step(table1)
    for i in table1:
        for j in i:
            j.scanned = False

def loadMap(player):
    f = open('level1.lvl', 'r')
    mapa = []
    y = 0
    fl = list(f)
    fl.reverse()
    for line in fl:
        x = 0
        print (y)
        mapa.append([])
        for i in line:
            print( x)
            if i == 'P':
                mapa[y].append(Door(player))
            elif i == 'r':
                mapa[y].append(Boulder(x, y))
            elif i == 'w':
                mapa[y].append(BrickWall())
            elif i == 'W':
                mapa[y].append(SteelWall())
            elif i == 'd':
                mapa[y].append(Diamond(x, y))
            elif i == 'X':
                mapa[y].append(player)
            elif i == '.':
                mapa[y].append(Dirt())
            elif i == ' ':
                mapa[y].append(Space())
            x += 1
        y += 1
    return mapa
def game(FPS):
    player = Rockford(3,19)
    table1 = loadMap(player)
    while player.gameState != GameState.exit:
        while player.gameState == GameState.playing:
            deltat = clock.tick(FPS*2)
            render(table1)
            deltat = clock.tick(FPS*2)
            render(table1)
            pygame.event.pump()
            keys = pygame.key.get_pressed()
            if keys[K_ESCAPE] != 0:
                player.gameState = GameState.exit
                break
            if keys[K_r] != 0:
                player.gameState = GameState.reset
                break
            if keys[K_SPACE] != 0:
                player.notMoving = True
            else:
                player.notMoving = False
            if keys[K_UP] != 0:
                player.state = RockfordState.goingUp
            elif keys[K_DOWN] != 0:
                player.state = RockfordState.goingDown
            elif keys[K_LEFT] != 0:
                player.state = RockfordState.goingLeft
            elif keys[K_RIGHT] != 0:
                player.state = RockfordState.goingRight
            else:
                player.state = RockfordState.none
            scan(table1)
            render(table1)
        while player.gameState == GameState.lose:
            screen.fill((0,0,0))
            screen.blit(lose, (0,0))
            pygame.display.flip()
            pygame.event.pump()
            keys = pygame.key.get_pressed()
            if keys[K_ESCAPE] != 0:
                player.gameState = GameState.exit
                break
            if keys[K_r] != 0:
                player.gameState = GameState.reset
                break

        while player.gameState == GameState.win:
            screen.fill((0,0,0))
            screen.blit(win, (0,0))
            pygame.display.flip()
            pygame.event.pump()
            keys = pygame.key.get_pressed()
            if keys[K_ESCAPE] != 0:
                player.gameState = GameState.exit
                break
            if keys[K_r] != 0:
                player.gameState = GameState.reset
                break
        if player.gameState == GameState.reset:
            player = Rockford(3,19)
            table1 = loadMap(player)
screen = pygame.display.set_mode((640,352), FULLSCREEN)
clock = pygame.time.Clock()
game(10)
pygame.display.quit()
pygame.quit()
exit()